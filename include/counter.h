#pragma once

extern int skinny64_64_blocks_counter(unsigned char *output,
			       unsigned char *input,
			       unsigned char *counter,
			       unsigned long long input_length,
			       const unsigned char *key);

void get_counter_input(unsigned char *counter, unsigned long long input_length, int blocks_number);
int skinny64_counter(unsigned char *output,
		     unsigned char *input,
		     unsigned char *counter,
		     unsigned long long input_length,
		     const unsigned char *key);
