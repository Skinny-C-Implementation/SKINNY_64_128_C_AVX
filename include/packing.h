#pragma once

void packing_16_blocks(u256 x[4], const unsigned char *in);
void unpacking_16_blocks(unsigned char *output, u256 x[4]);
void packing_64_blocks(u256 x[16], const unsigned char *input);
void unpacking_64_blocks(const unsigned char *output, u256 x[16]);
