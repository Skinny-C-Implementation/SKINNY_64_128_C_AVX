#pragma once

#include "immintrin.h"

/* Types */

#define u8 unsigned char
#define u64 unsigned long long
#define u256 __m256i

/* Types */


/* Intrinsics */

#define XOR _mm256_xor_si256
#define AND _mm256_and_si256
#define OR _mm256_or_si256
#define NOT(x) _mm256_xor_si256(x, _mm256_set_epi32(-1, -1, -1, -1, -1, -1, -1, -1)) //NOT = XOR between x and something fully filled with 1.
#define NOR(x, y) _mm256_xor_si256(_mm256_or_si256(x, y), _mm256_set_epi32(-1, -1, -1, -1, -1, -1, -1, -1)) // NOR = NOT(OR(x,y)) = OR(x,y) XOR something fully filled with 1.
#define NAND(x,y) _mm256_xor_si256(_mm256_and_si256(x, y), _mm256_set_epi32(-1, -1, -1, -1, -1, -1, -1, -1)) // NAND = NOT(AND(x,y)) = AND(x,y) XOR something fully filled with 1.
#define SHIFTR64(x, y) _mm256_srli_epi64(x, y)
#define SHIFTL64(x, y) _mm256_slli_epi64(x, y)
#define UNPACKHIGH8(x,y) _mm256_unpackhi_epi8(x, y)
#define UNPACKLOW8(x,y) _mm256_unpacklo_epi8(x, y)

#define SR_16_BLOCKS(x) _mm256_shuffle_epi8(x, _mm256_set_epi8(28, 31, 30, 29, 25, 24, 27, 26, 22, 21, 20, 23, 19, 18, 17, 16, 12, 15, 14, 13, 9, 8, 11, 10, 6, 5, 4, 7, 3, 2, 1, 0))
#define SR2L(x) _mm256_shuffle_epi8(x, _mm256_set_epi8(28, 29, 31, 30, 24, 25, 27, 26, 20, 21, 23, 22, 16, 17, 19, 18, 12, 13, 15, 14, 8, 9, 11, 10, 4, 5, 7, 6, 0, 1, 3, 2))
#define SR3L(x) _mm256_shuffle_epi8(x, _mm256_set_epi8(30, 31, 28, 29, 26, 27, 24, 25, 22, 23, 20, 21, 18, 19, 16, 17, 14, 15, 12, 13, 10, 11, 8, 9, 6, 7, 4, 5, 2, 3, 0, 1))
#define SR4L(x) _mm256_shuffle_epi8(x, _mm256_set_epi8(29, 28, 30, 31, 25, 24, 26, 27, 21, 20, 22, 23, 17, 16, 18, 19, 13, 12, 14, 15, 9, 8, 10, 11, 5, 4, 6, 7, 1, 0, 2, 3))
#define UPDKEY_16(x) _mm256_shuffle_epi8(x, _mm256_set_epi8(23, 22, 21, 20, 19, 18, 17, 16, 27, 28, 30, 26, 29, 24, 31, 25, 7, 6, 5, 4, 3, 2, 1, 0, 11, 12, 14, 10, 13, 8, 15, 9))

#define LOAD(src) _mm256_loadu_si256((__m256i *)(src))
#define STORE(dest,src) _mm256_storeu_si256((__m256i *)(dest),src)

#define SWAPMOVE(a, b, mask, shift)			\
  {							\
    u256 T = AND(XOR(SHIFTL64(a, shift), b), mask);	\
    b = XOR(b, T);					\
    a = XOR(a, SHIFTR64(T, shift));			\
  }

/* Intrinsics */


/* Masks */

#define MASK1 _mm256_set_epi32(0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa, 0xaaaaaaaa)
#define MASK2 _mm256_set_epi32(0xcccccccc, 0xcccccccc, 0xcccccccc, 0xcccccccc, 0xcccccccc, 0xcccccccc, 0xcccccccc, 0xcccccccc)
#define MASK4 _mm256_set_epi32(0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0, 0xf0f0f0f0)
#define MASK8 _mm256_set_epi32(0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00, 0xff00ff00)
#define MASK16 _mm256_set_epi32(0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000, 0xffff0000)
#define MASK32 _mm256_set_epi32(0xffffffff, 0x00000000, 0xffffffff, 0x00000000, 0xffffffff, 0x00000000, 0xffffffff, 0x00000000)

#define MASK1L _mm256_set_epi32(0x00000000, 0x00000000, 0x00000000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0xffffffff)
#define MASK3L _mm256_set_epi32(0x00000000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0xffffffff, 0x00000000, 0x00000000)
#define MASK12L _mm256_set_epi32(0x00000000, 0x00000000, 0xffffffff, 0xffffffff, 0x00000000, 0x00000000, 0xffffffff, 0xffffffff)

#define RC_64 _mm256_set_epi32(0x00ff0000, 0x00ff0000, 0x00ff0000, 0x000ff0000, 0x00ff0000, 0x00ff0000, 0x00ff0000, 0x00ff0000)
#define RC1 _mm256_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x000000ff, 0x00000000, 0x00000000, 0x00000000, 0x000000ff)
#define RC2 _mm256_set_epi32(0x00000000, 0x00000000, 0x000000ff, 0x00000000, 0x00000000, 0x00000000, 0x000000ff, 0x00000000)
#define RC3 _mm256_set_epi32(0x00000000, 0x000000ff, 0x00000000, 0x00000000, 0x00000000, 0x000000ff, 0x00000000, 0x00000000)

#define MASK16S _mm256_set_epi32(0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff, 0xff0000ff)

#define MC1 _mm256_set_epi8(31, 30, 29, 28, 19, 18, 17, 16, 27, 26, 25, 24, 23, 22, 21, 20, 15, 14, 13, 12, 3, 2, 1, 0, 11, 10, 9, 8, 7, 6, 5, 4)
#define MC2 _mm256_set_epi8(27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 31, 30, 29, 28, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 15, 14, 13, 12)

#define KS_64_1 _mm256_set_epi8(30, 28, 0, 0, 26, 24, 0, 0, 22, 20, 0, 0, 18, 16, 0, 0, 14, 12, 0, 0, 10, 8, 0, 0, 6, 4, 0, 0, 2, 0, 0, 0)
#define KS_64_2 _mm256_set_epi8(0, 0, 28, 29, 0, 0, 24, 25, 0, 0, 20, 21, 0, 0, 16, 17, 0, 0, 12, 13, 0, 0, 8, 9, 0, 0, 4, 5, 0, 0, 0, 1)
#define KS_64_3 _mm256_set_epi8(30, 0, 0, 31, 26, 0, 0, 27, 22, 0, 0, 23, 18, 0, 0, 19, 14, 0, 0, 15, 10, 0, 0, 11, 6, 0, 0, 7, 2, 0, 0, 3)
#define KS_64_4 _mm256_set_epi8(0, 31, 29, 0, 0, 27, 25, 0, 0, 23, 21, 0, 0, 19, 17, 0, 0, 15, 13, 0, 0, 11, 9, 0, 0, 7, 5, 0, 0, 3, 1, 0)

#define COMP_ZERO _mm256_set_epi32 (0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
#define COUNT64_128 _mm256_set_epi32(0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000)

/* Masks */


/* Optimization Macro for 16 blocks*/

#define SUBCELLS_16_BLOCKS(x)				\
  {							\
    tmp = XOR(x[3], NOR(x[0], x[1]));			\
    x[0] = XOR(x[0], NOR(x[1], x[2]));			\
    x[1] = XOR(x[1], NOR(x[2], tmp));			\
    x[2] = XOR(x[2], NOR(tmp, x[0]));			\
    x[3] = x[2];					\
    x[2] = x[1];					\
    x[1] = x[0];					\
    x[0] = tmp;						\
  }

#define ADD_CONST_AND_KEY_16_BLOCKS(x)		\
  {						\
    x[0] = XOR(x[0], rk[i][0]);			\
    x[1] = XOR(x[1], rk[i][1]);			\
    x[2] = XOR(x[2], rk[i][2]);			\
    x[3] = XOR(x[3], rk[i][3]);			\
  }

#define SHIFT_ROWS_16_BLOCKS(x)			\
  {						\
    x[0] = SR_16_BLOCKS(x[0]);			\
    x[1] = SR_16_BLOCKS(x[1]);			\
    x[2] = SR_16_BLOCKS(x[2]);			\
    x[3] = SR_16_BLOCKS(x[3]);			\
  }

#define MIX_COLUMNS_ITER_16_BLOCKS(x, j)				\
  {									\
    x[j] = XOR(x[j],  _mm256_shuffle_epi8(AND(x[j], XOR(MASK3L, MASK1L)), MC1)); \
    x[j] = XOR(x[j], SHIFTL64(AND(MASK3L, x[j]), 32));			\
    x[j] = _mm256_shuffle_epi8(x[j], MC2);				\
  }

#define MIX_COLUMNS_16_BLOCKS(x)		\
  {						\
    MIX_COLUMNS_ITER_16_BLOCKS(x, 0)		\
    MIX_COLUMNS_ITER_16_BLOCKS(x, 1)		\
    MIX_COLUMNS_ITER_16_BLOCKS(x, 2)		\
    MIX_COLUMNS_ITER_16_BLOCKS(x, 3)		\
  }

/* Optimization Macro for 16 blocks*/

/* Optimization Macro for 64 blocks*/

#define SUBCELLS_64_BLOCKS_ITER(x, j)			\
  {							\
    tmp = AND(x[1+4*j], x[2+4*j]);			\
    tmp = XOR(x[0+4*j], tmp);				\
    x[0+4*j] = AND(x[0+4*j], x[1+4*j]);			\
    x[0+4*j] = XOR(x[0+4*j], x[3+4*j]);			\
    x[3+4*j] = AND(x[2+4*j], x[0+4*j]);			\
    x[1+4*j] = XOR(x[1+4*j], x[3+4*j]);			\
    x[3+4*j] = AND(tmp, x[0+4*j]);			\
    x[2+4*j] = XOR(x[3+4*j], x[2+4*j]);			\
    x[3+4*j] = x[2+4*j];				\
    x[2+4*j] = x[1+4*j];				\
    x[1+4*j] = tmp;					\
  }
/*
  tmp = XOR(x[4*j+3], NOR(x[4*j+0], x[4*j+1]));			\
  x[4*j+0] = XOR(x[4*j+0], NOR(x[4*j+1], x[4*j+2]));		\
  x[4*j+1] = XOR(x[4*j+1], NOR(x[4*j+2], tmp));			\
  x[4*j+2] = XOR(x[4*j+2], NOR(tmp, x[4*j+0]));			\
  x[4*j+3] = x[4*j+2];						\
  x[4*j+2] = x[4*j+1];						\
  x[4*j+1] = x[4*j+0];						\
  x[4*j+0] = tmp;					\
*/

#define SUBCELLS_64_BLOCKS(x)			\
  {						\
    SUBCELLS_64_BLOCKS_ITER(x, 0);		\
    SUBCELLS_64_BLOCKS_ITER(x, 1);		\
    SUBCELLS_64_BLOCKS_ITER(x, 2);		\
    SUBCELLS_64_BLOCKS_ITER(x, 3);		\
  }

#define ADD_CONST_AND_KEY_64_BLOCKS_ITER(x, j)	\
  {						\
    x[j] = XOR(x[j], rk[i][j]);			\
    x[j+4] = XOR(x[j+4], rk[i][j+4]);		\
  }

#define ADD_CONST_AND_KEY_64_BLOCKS(x)		\
  {						\
    ADD_CONST_AND_KEY_64_BLOCKS_ITER(x, 0);	\
    ADD_CONST_AND_KEY_64_BLOCKS_ITER(x, 1);	\
    ADD_CONST_AND_KEY_64_BLOCKS_ITER(x, 2);	\
    ADD_CONST_AND_KEY_64_BLOCKS_ITER(x, 3);	\
    x[10] = XOR(x[10], RC_64);			\
  }
  
#define SHIFT_ROWS_64_BLOCKS_ITER(x, j)			\
  {							\
    x[4+j] = SR2L(x[4+j]);				\
    x[8+j]  = SR3L(x[8+j]);				\
    x[12+j] = SR4L(x[12+j]);				\
  }

#define SHIFT_ROWS_64_BLOCKS(x)				\
  {							\
    SHIFT_ROWS_64_BLOCKS_ITER(x, 0);			\
    SHIFT_ROWS_64_BLOCKS_ITER(x, 1);			\
    SHIFT_ROWS_64_BLOCKS_ITER(x, 2);			\
    SHIFT_ROWS_64_BLOCKS_ITER(x, 3);			\
  }

#define MIX_COLUMNS_64_BLOCKS_ITER(x,j)		\
  {						\
    x[4+j] = XOR(x[4+j], x[8+j]);		\
    x[8+j] = XOR(x[0+j], x[8+j]);		\
    tmp = XOR(x[8+j], x[12+j]);			\
    x[12+j] = x[8+j];				\
    x[8+j] = x[4+j];				\
    x[4+j] = x[0+j];				\
    x[0+j] = tmp;				\
  }

#define MIX_COLUMNS64_BLOCKS(x)			\
  {						\
    MIX_COLUMNS_64_BLOCKS_ITER(x,0);		\
    MIX_COLUMNS_64_BLOCKS_ITER(x,1);		\
    MIX_COLUMNS_64_BLOCKS_ITER(x,2);		\
    MIX_COLUMNS_64_BLOCKS_ITER(x,3);		\
  }

#define CPY_REG(x, y)				\
  {						\
    y[0] = x[0];				\
    y[1] = x[1];				\
    y[2] = x[2];				\
    y[3] = x[3];				\
    y[4] = x[4];				\
    y[5] = x[5];				\
    y[6] = x[6];				\
    y[7] = x[7];				\
    y[8] = x[8];				\
    y[9] = x[9];				\
    y[10] = x[10];				\
    y[11] = x[11];				\
    y[12] = x[12];				\
    y[13] = x[13];				\
    y[14] = x[14];				\
    y[15] = x[15];				\
  }
/* Optimization Macro for 64 blocks*/
