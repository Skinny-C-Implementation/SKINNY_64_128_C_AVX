#pragma once

extern int skinny64_64_blocks(unsigned char *output,
		       unsigned char *input,
		       unsigned long long input_length,
		       const unsigned char *key);

extern int skinny64_16_blocks(unsigned char *output,
		       unsigned char *input,
		       unsigned long long input_length,
		       const unsigned char *key);

int check_test_vector(int blocks_number);
