#pragma once

/* 16 BLOCKS*/

void encrypt_16_blocks(u256 x[4], u256 rk[36][4]);
void key_schedule_16_blocks(const unsigned char *key,
			    u256 rk[36][4]);
int skinny64_16_blocks(unsigned char *output,
		       unsigned char *input,
		       unsigned long long input_length,
		       const unsigned char *key);

/* 16 BLOCKS*/

/* 64 BLOCKS */

void encrypt_64_blocks(u256 x[16], u256 rk[36][8]);
void key_schedule_64_blocks(const unsigned char *key,
			    u256 rk[36][8]);
int skinny64_64_blocks(unsigned char *output,
		       unsigned char *input,
		       unsigned long long input_length,
		       const unsigned char *key);
int skinny64_64_blocks_counter(unsigned char *output,
			       unsigned char *input,
			       unsigned char *counter,
			       unsigned long long input_length,
			       const unsigned char *key);

/* 64 BLOCKS */
