#pragma once

#define NUM_TIMINGS 2000

extern int skinny64_16_blocks(unsigned char *output,
		       unsigned char *input,
		       unsigned long long input_length,
		       const unsigned char *key);

extern int skinny64_64_blocks(unsigned char *output,
		       unsigned char *input,
		       unsigned long long input_length,
		       const unsigned char *key);

void benchmark_cipher(int blocks_number);
void benchmark_counter();
int cmp_dbl(const void *x, const void *y);
void get_random_imput(unsigned char *input, unsigned char *key, unsigned long long input_length);
