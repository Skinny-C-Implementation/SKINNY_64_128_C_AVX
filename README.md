# Skinny-64-128 AVX implementation

## Notes
This implementation has been realised during and internship at the [SYLLAB laboratory](http://www1.spms.ntu.edu.sg/~syllab/m/index.php/Home)

[Reference paper](https://eprint.iacr.org/2016/660.pdf)

[Reference site](https://sites.google.com/site/skinnycipher/)

[Reference github](https://github.com/kste/skinny_avx/tree/e0e362eb590ebd158dc749d33d39d17af977495b)

The plain text in this version is 64 bits long, and the tweakey is 128 bits long. The tweakey is divided into 2 64 bits part.

The Skinny lightweight cipher is composed of 5 steps + 1 Tweakey Schedule:
- Sub Cells
- Add Constants
- Add Round Tweakey
- Shift Rows
- Mix Columns

This implementation benchmark the Skinny-64-128 cipher with and input of 16 random plaintexts at once. It is done 2000² times.

This version contains only the encryption. But the decryption is trivial.

The makefile contains the O3 option. If you want to erase it or use O2 you need to modify the following line in the makefile:
```bash
CFLAGS=-Wall -pedantic -g -std=gnu99 -O3 -I$(INCLUDEDIR) -I$(HOME)/local/include -lm
```

/!\ *IMPORTANT*: To execute the code, you need at least a 64bits x86 processor with the **A**dvanced **V**ector **E**xtensions (AVX) instruction set. /!\

## Use:

To compile the program on a Unix system, open a Terminal in the directory containing this repository's content.

```bash
make clean
make all
```

Note: If some directories are missing, you may need to redo the commands one more time.

Then, to use the program, do:

```bash
./bin/Skinny_64_128_AVX
```

The program will generate random plaintexts and benchmark the cipher. If you try to update something and that it is not validating the tests, the program will print and error.

## Results:
|Skinny-64-128 |16 blocks|64 blocks|Counter 64 blocks|
|:----------------------:|:-----------------:|:-----------------:|:----------------:|
|v1.0|9.82 cpb|-|-|
|v2.0|6.79 cpb|-|-|
|v3.0|6.35 - 6.45 cpb|-|-|
|v4.0|6.32 cpb|3.77 cpb|-|
|Current|6.32 cpb| 2.42 cpb| 2.13 cpb|
|Current without turbo boost|6.74 cpb|2.60 cpb|2.38 cpb|
