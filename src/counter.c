#include "avx_declaration.h"
#include "counter.h"

void get_counter_input(unsigned char *counter, unsigned long long input_length, int blocks_number){
  unsigned char *tmp = (unsigned char*) malloc(8*sizeof(u8));
  unsigned int rand_tmp_1 = rand();
  unsigned int rand_tmp_2 = rand();

  for(int k=0; k<(input_length/(blocks_number*8)); k++){
    for(int j=0; j<64; j++){
      for(int i=2; i>=0; i--){
	tmp[i] = (rand_tmp_1  & (0xff << 8*i)) >> 8*i;
      }
      for(int i=3; i>=0; i--){
	tmp[3+i] = (rand_tmp_2  & (0xff << 8*(3-i))) >> 8*(3-i);
      }
      tmp[7] = j;
      for(int i=0; i<8; i++){
	counter[512*k+8*j+i] = tmp[i];
      }
    }
    rand_tmp_2 += 1;
  }
  
  free(tmp);
}

int skinny64_counter(unsigned char *output,
		     unsigned char *input,
		     unsigned char *counter,
		     unsigned long long input_length,
		     const unsigned char *key){

  skinny64_64_blocks_counter(output, input, counter, input_length, key);

  return 0;
}
