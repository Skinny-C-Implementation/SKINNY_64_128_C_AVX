#include <string.h>
#include "avx_declaration.h"
#include "packing.h"
#include "skinny64.h"

const unsigned char RC[62] = {
  0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3E, 0x3D, 0x3B, 0x37, 0x2F,
  0x1E, 0x3C, 0x39, 0x33, 0x27, 0x0E, 0x1D, 0x3A, 0x35, 0x2B,
  0x16, 0x2C, 0x18, 0x30, 0x21, 0x02, 0x05, 0x0B, 0x17, 0x2E,
  0x1C, 0x38, 0x31, 0x23, 0x06, 0x0D, 0x1B, 0x36, 0x2D, 0x1A,
  0x34, 0x29, 0x12, 0x24, 0x08, 0x11, 0x22, 0x04, 0x09, 0x13,
  0x26, 0x0c, 0x19, 0x32, 0x25, 0x0a, 0x15, 0x2a, 0x14, 0x28,
  0x10, 0x20};

/* 16 BLOCKS*/

void encrypt_16_blocks(u256 x[4], u256 rk[36][4]){
  u256 tmp;
  for(int i=0; i<36; i++){
    SUBCELLS_16_BLOCKS(x);
    ADD_CONST_AND_KEY_16_BLOCKS(x);
    SHIFT_ROWS_16_BLOCKS(x);
    MIX_COLUMNS_16_BLOCKS(x);
  }
}

void key_schedule_16_blocks(const unsigned char *key, u256 rk[36][4]){
  u256 tk1[4], tk2[4], tmp;

  unsigned char *tmp_key = (unsigned char*)malloc(128*sizeof(u8));
  
  //Fill TK1 & TK2
  for(int i=0; i<16; i++){
    memcpy(tmp_key + 8*i, key, 8);
  }
  packing_16_blocks(tk1, tmp_key);

  for(int i=0; i<16; i++){
    memcpy(tmp_key + 8*i, key+8, 8);
  }
  packing_16_blocks(tk2, tmp_key);
  
  //Fill RK
  for(int i=0; i<36; i++){
    //Extract each round key
    for(int j=0; j<4; j++){
      rk[i][j] = XOR(AND(tk1[j], MASK12L), AND(tk2[j], MASK12L));
    }
    
    //Add constant to the key
    if(RC[i]>>5 & 1) //R2
      rk[i][2] = XOR(rk[i][2], RC2);
    if(RC[i]>>4 & 1) //R3
      rk[i][3] = XOR(rk[i][3], RC2);
    if(RC[i]>>3 & 1) //R0
      rk[i][0] = XOR(rk[i][0], RC1);
    if(RC[i]>>2 & 1) //R1
      rk[i][1] = XOR(rk[i][1], RC1);
    if(RC[i]>>1 & 1) //R2
      rk[i][2] = XOR(rk[i][2], RC1);
    if(RC[i]>>0 & 1) //R3
      rk[i][3] = XOR(rk[i][3], RC1);
    
    rk[i][2] = XOR(rk[i][2], RC3);

    //Update TK1 & TK2
    for(int j=0; j<4; j++){
      tk1[j] = UPDKEY_16(tk1[j]);
      tk2[j] = UPDKEY_16(tk2[j]);
    }
    
    //TK2 LFSR
    tmp = AND(XOR(tk2[0], tk2[1]), MASK12L);
    tk2[0] = XOR(AND(tk2[1], MASK12L), AND(tk2[0], NOT(MASK12L)));
    tk2[1] = XOR(AND(tk2[2], MASK12L), AND(tk2[1], NOT(MASK12L)));
    tk2[2] = XOR(AND(tk2[3], MASK12L), AND(tk2[2], NOT(MASK12L)));
    tk2[3] = XOR(tmp, AND(tk2[3], NOT(MASK12L)));
  }
  
  free(tmp_key);
}

int skinny64_16_blocks(unsigned char *output,
		       unsigned char *input,
		       unsigned long long input_length,
		       const unsigned char *key){
  u256 rk[36][4];
  u256 x[4];

  //Key Schedule
  key_schedule_16_blocks(key, rk);

  while(input_length >= 128){
    //Packing
    packing_16_blocks(x, input);
    //Encryption
    encrypt_16_blocks(x, rk);
    //Unpacking
    unpacking_16_blocks(output, x);

    input_length -= 128;
    input += 128;
    output += 128;
  }
  
  return 0;
}

/* 16 BLOCKS*/

/* 64 BLOCKS */

void encrypt_64_blocks(u256 x[16], u256 rk[36][8]){
  u256 tmp;
  
  for(int i=0; i<16; i++){
    x[i] = NOT(x[i]);
  }
  
  for(int i=0; i<36; i++){
    //Subcells
    SUBCELLS_64_BLOCKS(x);    
    
    //Add constant & key
    ADD_CONST_AND_KEY_64_BLOCKS(x);
    
    //ShiftRows
    SHIFT_ROWS_64_BLOCKS(x);
    
    //MixColumns
    MIX_COLUMNS64_BLOCKS(x);

    
    for(int j=0; j<8; j++){
      x[j+8] = NOT(x[j+8]);
    }
  }
  for(int i=0; i<16; i++){
    x[i] = NOT(x[i]);
  }
}

void key_schedule_64_blocks(const unsigned char *key,
			    u256 rk[36][8]){
  u256 tk1[16], tk2[16], tmp[16];

  unsigned char *tmp_key = (unsigned char*)malloc(512*sizeof(u8));
  
  //Fill TK1 & TK2
  for(int i=0; i<64; i++){
    memcpy(tmp_key + 8*i, key, 8);
  }
  packing_64_blocks(tk1, tmp_key);
  
  for(int i=0; i<64; i++){
    memcpy(tmp_key + 8*i, key+8, 8);
  }
  packing_64_blocks(tk2, tmp_key);
  
  //Fill RK
  for(int i=0; i<36; i++){
    //Extract each round key
    for(int j=0; j<8; j++){
      rk[i][j] = XOR(tk1[j], tk2[j]);
    }
    
    //Add constant to the key
    if(RC[i]>>5 & 1)
      rk[i][6] = XOR(rk[i][6], RC_64);
    if(RC[i]>>4 & 1)
      rk[i][7] = XOR(rk[i][7], RC_64);
    if(RC[i]>>3 & 1)
      rk[i][0] = XOR(rk[i][0], RC_64);
    if(RC[i]>>2 & 1)
      rk[i][1] = XOR(rk[i][1], RC_64);
    if(RC[i]>>1 & 1)
      rk[i][2] = XOR(rk[i][2], RC_64);
    if(RC[i]>>0 & 1)
      rk[i][3] = XOR(rk[i][3], RC_64);
    
    //Update TK1 & TK2
    for(int j=0; j<4; j++){
      tmp[0] = tk1[0+j];
      tmp[1] = tk1[4+j];
      tk1[0+j] = XOR(AND(MASK16, _mm256_shuffle_epi8(tk1[8+j], KS_64_1)),
		     AND(NOT(MASK16), _mm256_shuffle_epi8(tk1[12+j], KS_64_2)));
      tk1[4+j] = XOR(AND(MASK16S, _mm256_shuffle_epi8(tk1[12+j], KS_64_3)),
		     AND(NOT(MASK16S), _mm256_shuffle_epi8(tk1[8+j], KS_64_4)));
      tk1[8+j] = tmp[0];
      tk1[12+j] = tmp[1];
    }

    for(int j=0; j<4; j++){
      tmp[0] = tk2[0+j];
      tmp[1] = tk2[4+j];
      tk2[0+j] = XOR(AND(MASK16, _mm256_shuffle_epi8(tk2[8+j], KS_64_1)),
		     AND(NOT(MASK16), _mm256_shuffle_epi8(tk2[12+j], KS_64_2)));
      tk2[4+j] = XOR(AND(MASK16S, _mm256_shuffle_epi8(tk2[12+j], KS_64_3)),
		     AND(NOT(MASK16S), _mm256_shuffle_epi8(tk2[8+j], KS_64_4)));
      tk2[8+j] = tmp[0];
      tk2[12+j] = tmp[1];
    }
    
    //TK2 LFSR
    for(int j=0; j<2; j++){
      tmp[0] = XOR(tk2[4*j+0], tk2[4*j+1]);
      tk2[4*j+0] = tk2[4*j+1];
      tk2[4*j+1] = tk2[4*j+2];
      tk2[4*j+2] = tk2[4*j+3];
      tk2[4*j+3] = tmp[0];
    }
  }
  
  free(tmp_key);
}

int skinny64_64_blocks(unsigned char *output,
		       unsigned char *input,
		       unsigned long long input_length,
		       const unsigned char *key){
  u256 rk[36][8];
  u256 x[16];
  
  //Key Schedule
  key_schedule_64_blocks(key, rk);
  
  while(input_length >= 512){
    //Packing
    packing_64_blocks(x, input);
    //Encryption
    encrypt_64_blocks(x, rk);
    //Unpacking
    unpacking_64_blocks(output, x);

    input_length -= 512;
    input += 512;
    output += 512;
  }
  
  return 0;

}

int skinny64_64_blocks_counter(unsigned char *output,
			       unsigned char *input,
			       unsigned char *counter,
			       unsigned long long input_length,
			       const unsigned char *key){
  u256 rk[36][8];
  u256 x[16]; u256 y[16];
  char cmp = 0;

  //Key Schedule
  key_schedule_64_blocks(key, rk);

  while(input_length >= 512){
    //Packing
    packing_64_blocks(x, input);
    
    for(int i=0; i<4; i++){
      if(cmp != 0){
	x[13] = XOR(x[13], COUNT64_128);
      }
      if(cmp == 2){
	x[12] = XOR(x[12], COUNT64_128);
      }
      
      //Encryption
      encrypt_64_blocks(x, rk);
      //Unpacking
      CPY_REG(x,y);
      unpacking_64_blocks(output, y);
      
      cmp = (cmp+1)%4;
      input_length -= 512;
      output += 512;
    }
    counter += 512;
  }

  for(int i=0; i<input_length; i++){
    output[i] = output[i] ^ input[i];
  }
  
  return 0;
}

/* 64 BLOCKS */
