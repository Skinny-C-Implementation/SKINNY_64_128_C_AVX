#include <stdio.h>

#include "avx_declaration.h"
#include "benchmark.h"
#include "check_vectors.h"

int main(int argc, char *argv[]){
  //Set the random seed.
  srand(0);

  printf("Skinny 64-128 benchmarking starting\n");

  //Check if the test vector is OK
  if(check_test_vector(16)){
    //Call to the benchmark function
    benchmark_cipher(16);
  }
  else{
    printf("ERROR: Unable to process to the benchmarking. Tests are not ok. \n");
  }
  
  //Check if the test vector is OK  
  if(check_test_vector(64)){
    //Call to the benchmark function
    benchmark_cipher(64);
  }
  else{
    printf("ERROR: Unable to process to the benchmarking. Tests are not ok. \n");
  }

  //Call to the benchmark function
  benchmark_counter();
  
  printf("End\n");
}
