#include "avx_declaration.h"

/* 16 BLOCKS*/

void packing_16_blocks(u256 x[4], const unsigned char *input){
  //Variables
  u256 tmp[2];
  
  //Registries filling
  for(int i=0; i<4; i++){
    x[i] = LOAD(input+i*32);
  }
  
  //Group bits for S-Box
  SWAPMOVE(x[0], x[1], MASK1, 1);
  SWAPMOVE(x[2], x[3], MASK1, 1);
  SWAPMOVE(x[0], x[2], MASK2, 2);
  SWAPMOVE(x[1], x[3], MASK2, 2);
  
  //Unpack and interleaves bytes
  tmp[0] = UNPACKLOW8(x[0], x[1]);
  x[1] = UNPACKHIGH8(x[0], x[1]);
  tmp[1] = UNPACKLOW8(x[2], x[3]);
  x[3] = UNPACKHIGH8(x[2], x[3]);
  
  x[0] = tmp[0];
  x[2] = tmp[1];
    
  //Group nibble in bytes
  SWAPMOVE(x[0], x[1], MASK4, 4);
  SWAPMOVE(x[2], x[3], MASK4, 4);
  SWAPMOVE(x[1], x[0], MASK8, 8);
  SWAPMOVE(x[3], x[2], MASK8, 8);
}

void unpacking_16_blocks(unsigned char *output, u256 x[4]){
  u256 tmp[4];

  SWAPMOVE(x[1], x[0], MASK8, 8);
  SWAPMOVE(x[3], x[2], MASK8, 8);
  SWAPMOVE(x[0], x[1], MASK4, 4);
  SWAPMOVE(x[2], x[3], MASK4, 4);
  
  for(int i=0; i<2; i++){
    tmp[0] = UNPACKLOW8(x[0], x[1]);
    tmp[1] = UNPACKHIGH8(x[0], x[1]);
    tmp[2] = UNPACKLOW8(x[2], x[3]);
    tmp[3] = UNPACKHIGH8(x[2], x[3]);

    x[0] = UNPACKLOW8(tmp[0], tmp[1]);
    x[1] = UNPACKHIGH8(tmp[0], tmp[1]);
    x[2] = UNPACKLOW8(tmp[2], tmp[3]);
    x[3] = UNPACKHIGH8(tmp[2], tmp[3]);
  }
  
  SWAPMOVE(x[0], x[2], MASK2, 2);
  SWAPMOVE(x[1], x[3], MASK2, 2);
  SWAPMOVE(x[0], x[1], MASK1, 1);
  SWAPMOVE(x[2], x[3], MASK1, 1);

  for(int i=0; i<4; i++){
    STORE(output+32*i,  x[i]);
  }
}

/* 16 BLOCKS*/

/* 64 BLOCKS */

void packing_64_blocks(u256 x[16], const unsigned char *input){
  for(int i=0; i<16; i++) {
    x[i] = LOAD(input+i*32);
  }

  //Group bits
  for(int i=0; i<4; i++) {
    SWAPMOVE(x[4*i+0], x[4*i+1], MASK1, 1);
    SWAPMOVE(x[4*i+2], x[4*i+3], MASK1, 1);
    SWAPMOVE(x[4*i+0], x[4*i+2], MASK2, 2);
    SWAPMOVE(x[4*i+1], x[4*i+3], MASK2, 2);    
  }

  //Grouping nibbles into bytes
  for(int i=0; i < 4; i++){
    SWAPMOVE(x[i+4], x[i+0], MASK4, 4);
    SWAPMOVE(x[i+12], x[i+8], MASK4, 4);
  }
  
  //Grouping rows
  for(int i=0; i < 4; i++){
    SWAPMOVE(x[i+4], x[i+0], MASK16, 16);
    SWAPMOVE(x[i+12], x[i+8], MASK16, 16);
    SWAPMOVE(x[i+8], x[i+0], MASK32, 32);
    SWAPMOVE(x[i+12], x[i+4],MASK32, 32);    
  }
}

void unpacking_64_blocks(const unsigned char *output, u256 x[16]){
  for(int i=0; i<4; i++) {
    SWAPMOVE(x[i+4], x[i+0], MASK16, 16);
    SWAPMOVE(x[i+12], x[i+8], MASK16, 16);
    SWAPMOVE(x[i+8], x[i+0], MASK32, 32);
    SWAPMOVE(x[i+12], x[i+4],MASK32, 32);    
  }

  for(int i=0; i < 4; i++){
    SWAPMOVE(x[i+4], x[i+0], MASK4, 4);
    SWAPMOVE(x[i+12], x[i+8], MASK4, 4);
  }

  for(int i=0; i<4; i++) {
    SWAPMOVE(x[4*i+0], x[4*i+1], MASK1, 1);
    SWAPMOVE(x[4*i+2], x[4*i+3], MASK1, 1);
    SWAPMOVE(x[4*i+0], x[4*i+2], MASK2, 2);
    SWAPMOVE(x[4*i+1], x[4*i+3], MASK2, 2);    
  }
  
  for(int i=0; i<16; i++) {
    STORE(output+32*i,  x[i]);
  }
}

/* 64 BLOCKS */
