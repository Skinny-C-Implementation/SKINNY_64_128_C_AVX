#include <stdio.h>

#include "avx_declaration.h"
#include "benchmark.h"
#include "counter.h"
#include "timing.h"

void benchmark_cipher(int blocks_number){
  //Allocation
  unsigned long long input_length = blocks_number*8*NUM_TIMINGS;
  unsigned char *input = (unsigned char*) malloc(input_length*sizeof(u8));
  unsigned char *output  = (unsigned char*) malloc(input_length*sizeof(u8));
  unsigned char *key  = (unsigned char*) malloc(16*sizeof(u8));
  u64 timer = 0;
  double timings[NUM_TIMINGS];

  //Get random inputs
  for(int i =-100; i<NUM_TIMINGS; i++){
    get_random_imput(input, key, input_length);
    
    //Benchmark the encryption
    if(blocks_number == 16){
      timer = start_rdtsc();
      skinny64_16_blocks(output, input, input_length, key);
      timer = end_rdtsc() - timer;
    }
    else{
      timer = start_rdtsc();
      skinny64_64_blocks(output, input, input_length, key);
      timer = end_rdtsc() - timer;
    }
    
    //Fill the array with all the timings values
    if(i>=0 && i<NUM_TIMINGS){
      timings[i] = ((double)timer) / input_length;
    }
  }
  //Get the median
  qsort(timings, NUM_TIMINGS, sizeof(double), cmp_dbl);
  printf("Skinny-64-128 %d blocks: %f cycles per byte\n", blocks_number, timings[NUM_TIMINGS / 2]);

  //Free malloc
  free(input);
  free(output);
  free(key);
}

void benchmark_counter(){
  u64 timer = 0;
  double timings[NUM_TIMINGS];
  int blocks_number = 64;

  //Allocation
  unsigned long long input_length = blocks_number*8*NUM_TIMINGS;
  unsigned char *input = (unsigned char*) malloc(input_length*sizeof(u8));
  unsigned char *output  = (unsigned char*) malloc(input_length*sizeof(u8));
  unsigned char *counter  = (unsigned char*) malloc((input_length/4)*sizeof(u8));
  unsigned char *key  = (unsigned char*) malloc(16*sizeof(u8));

  //Get random inputs
  for(int i =-100; i<NUM_TIMINGS; i++){
    get_random_imput(input, key, input_length); 
    get_counter_input(counter, input_length/4, blocks_number);

    //Benchmark the encryption
    timer = start_rdtsc();
    skinny64_counter(output, input, counter, input_length, key);
    timer = end_rdtsc() - timer;
    
    //Fill the array with all the timings values
    if(i>=0 && i<NUM_TIMINGS){
      timings[i] = ((double)timer) / input_length;
    }
  }
  //Get the median
  qsort(timings, NUM_TIMINGS, sizeof(double), cmp_dbl);
  printf("Skinny-64-128 counter mode %d blocks: %f cycles per byte\n", blocks_number, timings[NUM_TIMINGS / 2]);

  //Free malloc
  free(counter);
  free(input);
  free(output);
  free(key);
}

int cmp_dbl(const void *x, const void *y)
{
  double xx = *(double*)x;
  double yy = *(double*)y;
  
  if(xx < yy){
    return -1;
  }
  
  if(xx > yy){
    return  1;
  }
  
  return 0;
}

void get_random_imput(unsigned char *input, unsigned char *key, unsigned long long input_length){
  for(int j=0; j<input_length; j++){
    input[j] = rand() & 0xff;
  }
  for(int j=0; j<16; j++){
    key[j] = rand() & 0xff;
  }
}
